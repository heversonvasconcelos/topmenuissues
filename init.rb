Redmine::Plugin.register :top_menu_issues do
  name 'Top Menu Issues plugin'
  author 'Heverson Vasconcelos'
  description 'This is a plugin for Redmine that creates a top menu link to issues page'
  version '0.0.1'
  menu :top_menu, :issues, { :controller => 'issues', :action => 'index', :project_id => @project, :set_filter => 1 }, :after => :projects,  :caption => :label_issue_plural
end
